const providedItemLinks = document.querySelectorAll('[data-provided-item]');
providedItemLinks.forEach(link => {
  link.addEventListener('click', () => {
    window.items = (window.items || []).concat(link.dataset.providedItem);
  });
});

const consumedItemLinks = document.querySelectorAll('[data-consumed-item]');
consumedItemLinks.forEach(link => {
  link.addEventListener('click', (event) => {
    const { consumedItem } = link.dataset;
    if (!(window.items || []).includes(consumedItem)) {
      event.preventDefault();
      alert(`${consumedItem} is missing!`);
      return;
    }

    // remove consumed item
    items.splice(items.indexOf(consumedItem), 1);
  });
});
