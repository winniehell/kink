package de.winniehell.kink.dsl

import de.winniehell.kink.Scene
import de.winniehell.kink.Story

fun scene(block: Scene.() -> Unit = {}): Scene {
    val scene = Scene()
    block(scene)
    return scene
}

fun Story.scene(block: Scene.() -> Unit = {}): Scene {
    val story = this@scene

    val scene = Scene()
    story.add(scene)
    block(scene)

    return scene
}
