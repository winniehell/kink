package de.winniehell.kink.dsl

import de.winniehell.kink.Story

inline fun story(name: String, crossinline block: Story.() -> Unit = {}): Story {
    val story = Story(name)
    block(story)
    return story
}
