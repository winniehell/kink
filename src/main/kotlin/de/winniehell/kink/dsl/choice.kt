package de.winniehell.kink.dsl

import de.winniehell.kink.Choice
import de.winniehell.kink.Scene

inline fun Scene.choice(crossinline block: Choice.() -> Unit) : Choice {
    val choice = Choice()
    add(choice)
    block(choice)
    return choice
}
