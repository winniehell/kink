package de.winniehell.kink.html

import de.winniehell.kink.Choice
import de.winniehell.kink.Scene
import de.winniehell.kink.Story
import kotlinx.html.*
import kotlinx.html.stream.appendHTML
import java.io.File
import java.io.PrintStream
import java.net.URI

fun Story.toHTML(outputFile: File, themeURI: URI = URI("theme.css")) {
    val story = this

    fun sceneId(index: Int) = "scene-$index"
    fun sceneId(scene: Scene) = sceneId(indexOf(scene))

    fun UL.renderChoice(choice: Choice) {
        li {
            a(classes = "btn", href = "#${sceneId(choice.link())}") {
                val remarks = mutableListOf<String>()
                if (choice.consumedItem != null) {
                    attributes["data-consumed-item"] = choice.consumedItem!!
                    remarks.add("consumes ${choice.consumedItem}")
                }
                if (choice.providedItem != null) {
                    attributes["data-provided-item"] = choice.providedItem!!
                    remarks.add("provides ${choice.providedItem}")
                }
                text(choice.text)

                if (!remarks.isEmpty()) {
                    noScript { text(" (${remarks.joinToString(", ")})") }
                }
            }
        }
    }

    fun BODY.renderScene(sceneIndex: Int, scene: Scene) {
        hr { }

        section(classes = "scene") {
            id = sceneId(sceneIndex)

            // make links work in old HTML clients (like w3m)
            a { attributes["name"] = sceneId(sceneIndex) }

            h2 { text(sceneIndex) }

            scene.text.trimIndent().split('\n').forEach { line ->
                p { text(line) }
            }

            ul(classes = "choices") {
                scene.forEach { choice -> renderChoice(choice) }
            }
        }
    }

    val printStream = PrintStream(outputFile.outputStream())
    printStream.appendln("<!DOCTYPE html>")
    printStream.appendHTML().html {
        head {
            meta { charset = "utf-8" }
            meta { name = "generator"; content = "kotlin" }
            meta { name = "viewport"; content = "width=device-width, initial-scale=1.0, user-scalable=yes" }

            link {
                rel = LinkRel.stylesheet
                type = LinkType.textCss
                href = themeURI.toString()
            }

            title(story.name)
        }

        body(classes = "story") {
            noScript {
                text("You are viewing this page with JavaScript disabled. That means you will need to keep track of all consumed and provided items yourself.")
            }

            section(classes = "scene") {
                h1 { text(story.name) }
                a(classes = "btn", href = "#${sceneId(0)}") {
                    text("Start")
                }
            }

            story.forEachIndexed { sceneIndex, scene -> renderScene(sceneIndex, scene) }

            script(type = ScriptType.textJavaScript, src = "frontend.js") {}
        }
    }
}
