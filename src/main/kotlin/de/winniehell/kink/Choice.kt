package de.winniehell.kink

class Choice {
    var text = ""
    var link: () -> Scene = { throw Error("Missing link!") }
    var consumedItem : String? = null
    var providedItem : String? = null
}
