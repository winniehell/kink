package de.winniehell.kink.examples.multifile

import de.winniehell.kink.Scene
import de.winniehell.kink.dsl.choice
import de.winniehell.kink.dsl.scene

val first: Scene = scene {
    text = "This is the first scene."
    choice {
        text = "Second scene"
        link = { second }
    }
}
