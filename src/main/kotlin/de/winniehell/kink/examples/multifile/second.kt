package de.winniehell.kink.examples.multifile

import de.winniehell.kink.Scene
import de.winniehell.kink.dsl.choice
import de.winniehell.kink.dsl.scene

val second: Scene = scene {
    text = "This is the second scene."
    choice {
        text = "Back to first"
        link = { first }
    }
}
