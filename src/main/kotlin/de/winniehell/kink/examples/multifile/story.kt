package de.winniehell.kink.examples.multifile

import de.winniehell.kink.dsl.story
import de.winniehell.kink.html.toHTML
import java.io.File

fun main() {
    val myStory = story("Story with multiple files")
    myStory.addAll(
        listOf(
            first,
            second
        )
    )
    myStory.toHTML(File("public/multifile.html"))
}
