package de.winniehell.kink.examples

import de.winniehell.kink.examples.multifile.main as multifileExample
import de.winniehell.kink.examples.simple.main as simpleExample
import de.winniehell.kink.examples.item.main as itemExample

fun main() {
    simpleExample()
    multifileExample()
    itemExample()
}
