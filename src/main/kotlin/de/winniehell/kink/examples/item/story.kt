package de.winniehell.kink.examples.item

import de.winniehell.kink.Scene
import de.winniehell.kink.dsl.choice
import de.winniehell.kink.dsl.scene
import de.winniehell.kink.dsl.story
import de.winniehell.kink.html.toHTML
import java.io.File

fun main() {
    story("A conditional story") {
        var eatCake : Scene? = null
        var makeCake : Scene? = null

        val firstScene = scene {
            text = "It is your birthday."
            choice {
                text = "Eat cake"
                link = { eatCake!! }
                consumedItem = "cake"
            }
            choice {
                text = "Make cake"
                link = { makeCake!! }
            }
        }

        eatCake = scene {
            text = "The cake is delicious."
        }

        makeCake = scene {
            text = """
                Bake a cake, bake a cake,
                The baker has called.
                Whoever wants to bake a good cake,
                must have seven things,
                Eggs and lard,
                Sugar and salt,
                Milk and flour.
                Saffron makes the cake yellow
                Push it into the oven.
            """
            choice {
                text = "Push"
                link = { firstScene }
                providedItem = "cake"
            }
        }
    }.toHTML(File("public/item.html"))
}
