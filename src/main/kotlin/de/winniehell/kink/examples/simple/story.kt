package de.winniehell.kink.examples.simple

import de.winniehell.kink.dsl.choice
import de.winniehell.kink.dsl.scene
import de.winniehell.kink.dsl.story
import de.winniehell.kink.html.toHTML
import java.io.File

fun main() {
    story("A simple story") {
        scene {
            text = "This is the first scene."
            choice {
                text = "Last"
                link = this@story::last
            }
        }

        scene {
            text = "This is the last scene."
            choice {
                text = "First"
                link = this@story::first
            }
        }
    }.toHTML(File("public/simple.html"))
}
